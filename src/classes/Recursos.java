/*
Aqui encontrasse todos os recursos do jogo para criação dos personagens e deus metodos
*/
package classes;
public class Recursos {
  
   private static int madeira = 600;
   private static int ferro = 600;
   private static int dinheiro = 1500;
   private static int comida = 1500;

    public int getMadeira() {
        return madeira;
    }

    public int getFerro() {
        return ferro;
    }

    public int getDinheiro() {
        return dinheiro;
    }

    public int getComida() {
        return comida;
    }

    public void setMadeira(int madeira) {
        this.madeira = madeira;
    }

    public void setFerro(int ferro) {
        this.ferro = ferro;
    }

    public void setDinheiro(int dinheiro) {
        this.dinheiro = dinheiro;
    }

    public void setComida(int comida) {
        this.comida = comida;
    }
   
    public boolean diminuirComida(int quantidade)
    {
        if(comida - quantidade > 0)
        {
            this.comida = comida - quantidade;
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public boolean diminuirMadeira(int quantidade)
    {
       if(madeira - quantidade > 0)
        {
            this.madeira = madeira - quantidade;
            return true;
        }
        else
        {
            return false;
        } 
    }
    
    public boolean diminuirFerro(int quantidade)
    {
        if(ferro - quantidade > 0)
        {
            this.ferro = ferro - quantidade;
            return true;
        }
        else
        {
            return false;
        }
        
    }
   public boolean diminuirDinheiro(int quantidade)
    {
        if(dinheiro - quantidade > 0)
        {
            this.dinheiro = dinheiro - quantidade;
            return true;
        }
        else
        {
            return false;
        }
    }
}
