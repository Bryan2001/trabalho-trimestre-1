/*
Todas as funções do personagem 4 e seus atributos;
*/
package classes;
public class Personagem4{
    private static int quantidade = 0;
    private static int vida= 100;
    private static int ataque = 45;
    private int abilidade = 4;
    
    public int getQuantidade() {
        return quantidade;
    }

    public void Aumentar () {
        quantidade = quantidade + 1;
    }
    public void Diminuir () {
        quantidade = quantidade - 1;
    }
    
    public int getVida() {
        return vida;
    }

    public int getAtaque() {
        return ataque;
    }

    public void VidaA() {
        vida = vida + 2;
    }
    public void VidaD() {
        vida = vida - 2;
    }

    public void AtaqueA() {
        ataque = ataque + 4;
    }
    public void AtaqueD() {
        ataque = ataque - 4;
    }
}
