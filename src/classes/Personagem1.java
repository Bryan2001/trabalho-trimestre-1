/*
Todas as funções do personagem 1 e seus atributos;
*/

package classes;

import TrabalhoTrimestre1.Personagem1_ModificarController;

public class Personagem1{
    static Personagem1_ModificarController pm = new Personagem1_ModificarController();
    private static int quantidade = 0;
    private static int vida= 125 + (pm.getDefesa()* 2);
    private static int ataque = 35+ (pm.getAtaque()* 1);
    private static int abilidade = 1;
    
    public int getQuantidade() {
        return quantidade;
    }

    public void Aumentar () {
        quantidade = quantidade + 1;
    }
    
    public void Diminuir () {
        quantidade = quantidade - 1;
    }

    public int getVida() {
        return vida;
    }

    public int getAtaque() {
        return ataque;
    }

    
}
