/*
Aqui encontrasse as tropas inimigas e seus metodos
*/
package classes;
import java.util.ArrayList;

public class Tropa {
    private static int q1,q2,q3,q4;
    
    public static ArrayList <Personagem1> inimigo1 = new ArrayList<>();
    public static ArrayList <Personagem2> inimigo2 = new ArrayList<>();
    public static ArrayList <Personagem3> inimigo3 = new ArrayList<>();
    public static ArrayList <Personagem4> inimigo4 = new ArrayList<>();
    
    public void add1(Personagem1 p1){
        for(int i=0; i<3; i++){
            inimigo1.add(p1);
        }
        q1 = inimigo1.size();
        System.out.println("Inimigos Guerreiros: " + q1);
       }
    public int size1(){
        return q1 = inimigo1.size();
    }
    
    public int getVida1() {
        return inimigo1.get(0).getVida();
    }

    public int getAtaque1() {
        return inimigo1.get(0).getAtaque();
    }
    
    public void remove(){
        inimigo1.remove(0);
        q1=q1-1;
    }
    public void add2(Personagem2 p2){
        for(int i=0; i<3; i++){
            inimigo2.add(p2);
        }
        q2 = inimigo1.size();
        System.out.println("Inimigos Atiradoras: " + q2);
       }
    public int size2(){
        return q2 = inimigo2.size();
    }
    
    public int getVida2() {
        return inimigo2.get(0).getVida();
    }

    public int getAtaque2() {
        return inimigo2.get(0).getAtaque();
    }
    
    public void remove2(){
        inimigo2.remove(0);
        q2=q2-1;
    }
    
    public void add3(Personagem3 p3){
        for(int i=0; i<3; i++){
            inimigo3.add(p3);
        }
        q3 = inimigo3.size();
        System.out.println("Inimigos Medicos: " + q3);
       }
    public int size3(){
        return q3 = inimigo3.size();
    }
    
    public int getVida3() {
        return inimigo3.get(0).getVida();
    }

    public int getAtaque3() {
        return inimigo3.get(0).getAtaque();
    }
    
    public void remove3(){
        inimigo3.remove(0);
        q3= q3-1;
    }
    
    public void add4(Personagem4 p4){
        for(int i=0; i<3; i++){
            inimigo4.add(p4);
        }
        q4 = inimigo4.size();
        System.out.println("Inimigos Infiltradoras: " + q4);
       }
    public int size4(){
        return q4 = inimigo4.size();
    }
    
    public int getVida4() {
        return inimigo4.get(0).getVida();
    }

    public int getAtaque4() {
        return inimigo4.get(0).getAtaque();
    }
    
    public void remove4(){
        inimigo4.remove(0);
        q4=q4-1;
    }
}
