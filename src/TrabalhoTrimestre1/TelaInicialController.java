package TrabalhoTrimestre1;

import classes.*;
import java.net.URL;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

public class TelaInicialController implements Initializable {
    private Recursos r = new Recursos();
    private Personagem1 p1 = new Personagem1();
    private Personagem2 p2 = new Personagem2();
    private Personagem3 p3 = new Personagem3();
    private Personagem4 p4 = new Personagem4();
    TeladeBatalhaController batalha = new TeladeBatalhaController();
    @FXML private Label quantidade_comida;
    @FXML private Label quantidade_ferro;
    @FXML private Label quantidade_madeira;
    @FXML private Label quantidade_dinheiro;
    @FXML private Label MostrarPersonagem1;
    @FXML private Label MostrarPersonagem2;
    @FXML private Label MostrarPersonagem3;
    @FXML private Label MostrarPersonagem4;
    @FXML private Label Mensagem;
    @FXML private Label Dano1;
    @FXML private Label Vida1;
    @FXML private Label Dano2;
    @FXML private Label Vida2;
    @FXML private Label Dano3;
    @FXML private Label Vida3;
    @FXML private Label Dano4;
    @FXML private Label Vida4;
    /**
     * Método inicializador dos recursos mostrados na tela após criar tropa.
     */
    public void Recalcular ()
    {
        quantidade_comida.setText(" " + r.getComida());
        quantidade_ferro.setText(" " + r.getFerro());
        quantidade_madeira.setText(" " + r.getMadeira());
        quantidade_dinheiro.setText(" " + r.getDinheiro());
    }
    /**
     * Método inicializador do personagem na tela. 
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.Recalcular();
        MostrarPersonagem1.setText(" " + p1.getQuantidade() +" ");
        MostrarPersonagem2.setText(" " + p2.getQuantidade() +" ");
        MostrarPersonagem3.setText(" " + p3.getQuantidade() +" ");
        MostrarPersonagem4.setText(" " + p4.getQuantidade() +" ");
        Dano1.setText("Dano: " + p1.getAtaque());
        Dano2.setText("Dano: " + p2.getAtaque());
        Dano3.setText("Dano: " + p3.getAtaque());
        Dano4.setText("Dano: " + p4.getAtaque());
        Vida1.setText("Vida: " + p1.getVida());
        Vida2.setText("Vida: " + p2.getVida());
        Vida3.setText("Vida: " + p3.getVida());
        Vida4.setText("Vida: " + p4.getVida()); 
        
    }    
    /**
     * São os botões que fazem a troca de tela da tela inicial com uns dos 4 personagens 
     */
    @FXML
    public void Configurar_personagem1() throws IOException{
        Play.trocaTela("Personagem1.fxml");
    }
    @FXML
    public void Configurar_personagem2() throws IOException{
        Play.trocaTela("Personagem2.fxml");
    }
    @FXML
    public void Configurar_personagem3(ActionEvent event) {
        Play.trocaTela("Personagem3.fxml");
    }
    @FXML
    public void Configurar_personagem4() throws IOException{
        Play.trocaTela("Personagem4.fxml");
    }
    @FXML
     /**
     * Criador do personagem. 1
     * @param event.
     */
    public void CriarPersonagem1(ActionEvent event) {
        if(r.diminuirComida(40) && r.diminuirDinheiro(40) && r.diminuirFerro(15) && r.diminuirMadeira(10))
        {
        r.diminuirComida(40);
        r.diminuirDinheiro(40);
        r.diminuirFerro(15);
        r.diminuirMadeira(10);
        p1.Aumentar();
        batalha.add1();
        
        this.Recalcular();
        MostrarPersonagem1.setText(" " + p1.getQuantidade() +" ");
        }
        else
        {
            Mensagem.setText("Não é possivel fabricar mais tropas, pois faltou recursos");
        }
        
        
    }
    /**
     * Criador do personagem. 2
     * @param event.
     */
    @FXML
    public void CriarPersonagem2(ActionEvent event) {
        if(r.diminuirComida(35) && r.diminuirDinheiro(35) && r.diminuirFerro(15) && r.diminuirMadeira(20))
        {
        r.diminuirComida(35);
        r.diminuirDinheiro(35);
        r.diminuirFerro(10);
        r.diminuirMadeira(15);
        p2.Aumentar();
        this.Recalcular();
        batalha.add2();
        MostrarPersonagem2.setText(" " + p2.getQuantidade() +" ");
        }
        else
        {
            Mensagem.setText("Não é possivel fabricar mais tropas, pois faltou recursos");
        }
    }
    /**
     * Criador do personagem. 3
     * @param event.
     */
    @FXML
    public void CriarPersonagem3(ActionEvent event) {
        if(r.diminuirComida(40) && r.diminuirDinheiro(40) && r.diminuirFerro(10) && r.diminuirMadeira(10))
        {
        r.diminuirComida(40);
        r.diminuirDinheiro(40);
        r.diminuirFerro(10);
        r.diminuirMadeira(10);
        p3.Aumentar();
        this.Recalcular();
        batalha.add3();
        MostrarPersonagem3.setText(" " + p3.getQuantidade() +" ");
        }
        
        else
        {
            Mensagem.setText("Não é possivel fabricar mais tropas, pois faltou recursos");
        }
    }
     /**
     * Criador do personagem. 4
     * @param event.
     */
    @FXML
    public void CriarPersonagem4(ActionEvent event) {
        if(r.diminuirComida(35) && r.diminuirDinheiro(35) && r.diminuirFerro(15) && r.diminuirMadeira(10))
        {
        r.diminuirComida(35);
        r.diminuirDinheiro(35);
        r.diminuirFerro(15);
        r.diminuirMadeira(10);
        p4.Aumentar();
        this.Recalcular();
        batalha.add4();
        MostrarPersonagem4.setText(" " + p4.getQuantidade() +" ");
        }
        else
        {
            Mensagem.setText("Não é possivel fabricar mais tropas, pois faltou recursos");
        }
    }
    @FXML
    public void Batalhar() throws IOException{
        Play.trocaTela("TeladeBatalha.fxml");
    }
}
