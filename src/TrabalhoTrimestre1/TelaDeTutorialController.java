/*
Esse é o controler da tela do turorial
*/

package TrabalhoTrimestre1;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

public class TelaDeTutorialController implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
     @FXML
    public void  Voltar() throws IOException{
        Play.trocaTela("Tela Abertura.fxml");
    }
    
    
}
