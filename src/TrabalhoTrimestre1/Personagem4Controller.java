package TrabalhoTrimestre1;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

public class Personagem4Controller implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void Modificar_personagem4() throws IOException{
        Play.trocaTela("Personagem4_Modificar.fxml");
    }
    
    public void Voltar() throws IOException{
        Play.trocaTela("TelaInicial.fxml");
    }
    
    
}
