package TrabalhoTrimestre1;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
/**
* Classe da modificação do personagem 3. 
*/
public class Personagem3_ModificarController implements Initializable {
    
    private int  ataque = 0,defesa = 0, quantidade = 10;
    
    @FXML
    private Label Quantidade;

    @FXML
    private Label Ataque;

    @FXML
    private Label Defesa;
    
    
    @FXML
    private Label Mensagem;
    @FXML
    private Button Voltar;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Quantidade.setText("" + quantidade);
        Ataque.setText("" + ataque);
        Defesa.setText("" + defesa);
    }    
    
    /**
    * Método para aumento de ataque. 
    * @param event.
    */
    @FXML
    public void AumentarA(ActionEvent event) {
        
        if(quantidade >0)
        {
            quantidade = quantidade - 1;
            ataque = ataque + 1;
            Ataque.setText("" + ataque);
            Quantidade.setText("" + quantidade);
        }
        else
        {
            Mensagem.setText("Acabou seu pontos");
        }
    }
    /**
    * Método para aumento de defesa. 
    * @param event.
    */
    @FXML
    public void AumentarD(ActionEvent event) {
        if(quantidade >0)
        {
            quantidade = quantidade - 1;
            defesa = defesa + 1;
            Defesa.setText("" + defesa);
            Quantidade.setText("" + quantidade);
        }
        else
        {
            Mensagem.setText("Acabou seu pontos");
        }
    }
/**
    * Método para diminuição de ataque. 
    * @param event.
    */
    @FXML
    public void DiminuirA(ActionEvent event) {
        if(ataque > 0 )
        {
            quantidade = quantidade + 1;
            ataque = ataque - 1;
            Ataque.setText("" + ataque);
            Quantidade.setText("" + quantidade); 
        }
        else
        {
            Mensagem.setText("Não pode retirar mais pontos");
        }
        
    }
            /**
    * Método para diminuição de defesa. 
    * @param event.
    */
    @FXML
    public void DiminuirD(ActionEvent event) {
        if(defesa > 0 )
        {
            quantidade = quantidade + 1;
            defesa = defesa - 1;
            Defesa.setText("" + defesa);
            Quantidade.setText("" + quantidade);
        }
        else
        {
            Mensagem.setText("Não pode retirar mais pontos");
        }
    }
    
    @FXML
    public void Voltar() throws IOException{
        Play.trocaTela("TelaInicial.fxml");
    }
    
}
