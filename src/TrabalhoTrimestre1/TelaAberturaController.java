package TrabalhoTrimestre1;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

public class TelaAberturaController implements Initializable {
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
/**
 * Método para troca de tela. - Começar.
 * @throws IOException 
 */
    
    @FXML
    public void Começar() throws IOException{
        Play.trocaTela("TelaInicial.fxml");
    }
/**
 * Método para troca de tela. - Tutorial
 * @throws IOException 
 */
    @FXML
    public void  Tutorial() throws IOException{
        Play.trocaTela("Tela de Tutorial.fxml");
    }
/**
 * Método para troca de tela. - Sair.
 * @throws IOException 
 */
    @FXML
    public void Sair() throws IOException{
        //System.out.println("Aqui");
        System.exit(0);
    }
    
}
