package TrabalhoTrimestre1;
import classes.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

public class TeladeBatalhaController implements Initializable {
    //declaração
    @FXML private Label Mensagem;
    private Random random = new Random();
    private int numero = random.nextInt(1);
    private Personagem1 personagem1 = new Personagem1();
    private Personagem2 personagem2 = new Personagem2();
    private Personagem3 personagem3 = new Personagem3();
    private Personagem4 personagem4 = new Personagem4();
    private Tropa inimigo = new Tropa();
    private static ArrayList<Personagem1> aliado1 = new ArrayList<>();
    private static ArrayList<Personagem2> aliado2 = new ArrayList<>();
    private static ArrayList<Personagem3> aliado3 = new ArrayList<>();
    private static ArrayList<Personagem4> aliado4 = new ArrayList<>();
    private int verificador=2, q1,q2,q3,q4, i1,i2,i3,i4,x, valorI, valorA,a;
    private boolean y;
    private int p1[] = new int[2];
    private int p2[] = new int[2];
    //Funções
    @Override public void initialize(URL url, ResourceBundle rb) {
        System.out.println("Aliados: " + aliado1.size());
        q1= aliado1.size();
        q2= aliado2.size();
        q3= aliado3.size();
        q4= aliado4.size();
        inimigo.add1(personagem1);
        inimigo.add2(personagem2);
        inimigo.add3(personagem3);
        inimigo.add4(personagem4);
        i1= inimigo.size1();
        i2= inimigo.size2();
        i3= inimigo.size3();
        i4= inimigo.size4();
                
    }
    @FXML public void Voltar(ActionEvent event) {Play.trocaTela("TelaInicial.fxml");}
    void add1() {aliado1.add(personagem1);}
    void add2() {aliado2.add(personagem2);}
    void add3() {aliado3.add(personagem3);}
    void add4() {aliado4.add(personagem4);}
    
     /**
     * Método que mostra o resultado da batalha.
     */
        public void resultado(){
            if(verificador== 1)
        {
            System.out.println("Venceu");
            Mensagem.setText("Parabens, você venceu" + "\nVocê ajudou o meu Rio Grande a virar independente");
            this.removerI();
            verificador = 3;
            //System.out.println("aliado - " + q1 +"\n inimigo - " + i1);
        }
            else if(verificador == 2)
        {
           System.out.println("Perdeu");
           Mensagem.setText("Você perdeu" + "\nO estado de RS não ficou independente do Brasil");
           this.removerA();
           verificador = 3;
            //System.out.println("aliado - " + q1 +"\n inimigo - " + i1);

        }
        }
     /**
     * Método que inseri os personagens que irão fazer a batalha.
     */
        public void inicializar11(){
            
            p1[0] = aliado1.get(q1-1).getAtaque();
            p2[0] = inimigo.getAtaque1();
            p1[1] = aliado1.get(q1-1).getVida();
            p2[1] = inimigo.getVida1();
            valorI=1;
            valorA=1;
        }
        public void inicializar12(){
            p1[0] = aliado1.get(q1-1).getAtaque();
            p2[0] = inimigo.getAtaque2();
            p1[1] = aliado1.get(q1-1).getVida();
            p2[1] = inimigo.getVida2();
            valorI=2;
            valorA=1;
        }
        public void inicializar13(){
            p1[0] = aliado1.get(q1-1).getAtaque();
            p2[0] = inimigo.getAtaque3();
            p1[1] = aliado1.get(q1-1).getVida();
            p2[1] = inimigo.getVida3();
            valorA=1;
            valorI=3;
        }
        public void inicializar14(){
            p1[0] = aliado1.get(q1-1).getAtaque();
            p2[0] = inimigo.getAtaque4();
            p1[1] = aliado1.get(q1-1).getVida();
            p2[1] = inimigo.getVida4();
            valorA=1;
            valorI=4;
        }
        public void inicializar21(){
            p1[0] = aliado2.get(q2-1).getAtaque();
            p2[0] = inimigo.getAtaque1();
            p1[1] = aliado2.get(q2-1).getVida();
            p2[1] = inimigo.getVida1();
            valorA=2;
            valorI=1;
        }
        public void inicializar22(){
            p1[0] = aliado2.get(q2-1).getAtaque();
            p2[0] = inimigo.getAtaque2();
            p1[1] = aliado2.get(q2-1).getVida();
            p2[1] = inimigo.getVida2();
            valorA=2;
            valorI=2;
        }
        public void inicializar23(){
            p1[0] = aliado2.get(q2-1).getAtaque();
            p2[0] = inimigo.getAtaque3();
            p1[1] = aliado2.get(q2-1).getVida();
            p2[1] = inimigo.getVida3();
            valorA=2;
            valorI=3;
        }
        public void inicializar24(){
            p1[0] = aliado2.get(q2-1).getAtaque();
            p2[0] = inimigo.getAtaque4();
            p1[1] = aliado2.get(q2-1).getVida();
            p2[1] = inimigo.getVida4();
            valorA=2;
            valorI=4;
        }
        public void inicializar31(){
            p1[0] = aliado3.get(q3-1).getAtaque();
            p2[0] = inimigo.getAtaque1();
            p1[1] = aliado3.get(q3-1).getVida();
            p2[1] = inimigo.getVida1();
            valorA=3;
            valorI=1;
        }
        public void inicializar32(){
            p1[0] = aliado3.get(q3-1).getAtaque();
            p2[0] = inimigo.getAtaque2();
            p1[1] = aliado3.get(q3-1).getVida();
            p2[1] = inimigo.getVida2();
            valorA=3;
            valorI=2;
        }
        public void inicializar33(){
            p1[0] = aliado3.get(q3-1).getAtaque();
            p2[0] = inimigo.getAtaque3();
            p1[1] = aliado3.get(q3-1).getVida();
            p2[1] = inimigo.getVida3();
            valorA=3;
            valorI=3;
        }
        public void inicializar34(){
            p1[0] = aliado3.get(q3-1).getAtaque();
            p2[0] = inimigo.getAtaque4();
            p1[1] = aliado3.get(q3-1).getVida();
            p2[1] = inimigo.getVida4();
            valorA=3;
            valorI=4;
        }
        public void inicializar41(){
            p1[0] = aliado4.get(q4-1).getAtaque();
            p2[0] = inimigo.getAtaque1();
            p1[1] = aliado4.get(q4-1).getVida();
            p2[1] = inimigo.getVida1();
            valorA=4;
            valorI=1;
        }
        public void inicializar42(){
            p1[0] = aliado4.get(q4-1).getAtaque();
            p2[0] = inimigo.getAtaque2();
            p1[1] = aliado4.get(q4-1).getVida();
            p2[1] = inimigo.getVida2();
            valorA=4;
            valorI=2;
        }
        public void inicializar43(){
            p1[0] = aliado4.get(q4-1).getAtaque();
            p2[0] = inimigo.getAtaque3();
            p1[1] = aliado4.get(q4-1).getVida();
            p2[1] = inimigo.getVida3();
            valorA=4;
            valorI=3;
        }
        public void inicializar44(){
            p1[0] = aliado4.get(q4-1).getAtaque();
            p2[0] = inimigo.getAtaque4();
            p1[1] = aliado4.get(q4-1).getVida();
            p2[1] = inimigo.getVida4();
            valorA=4;
            valorI=4;
        }
     /**
     * Método que faz a batalha.
     */
        public void batalha(){
        int i = random.nextInt(2);
        switch(i){
        
        case 1 :
            if(p2[1] - p1[0] <= 0)
            {//Aliado ataca primeiro
            p2[1]=p2[1]- p1[0]; 
            p1[1]=p1[1]- p2[0];
            verificador = 1;
            x=q1;
            }
            
             else
                {
                  p2[1]=p2[1]- p1[0]; 
            p1[1]=p1[1]- p2[0];  
            i = random.nextInt(1);
                }
                break;
            
            case 0:
            if(p1[1] - p2[0] <= 0)
            {//Aliado ataca primeiro
            p1[1]=p1[1]- p2[0];
            p2[1]=p2[1]- p1[0]; 
            verificador = 2;
            x=i1;
            }
            
            else
                {
                p1[1]=p1[1]- p2[0];
                p2[1]=p2[1]- p1[0]; 
                i = random.nextInt(1);
                }
                break;
            }
        
        }
        public void removerI(){
                switch(valorI)
                {
                    case 1:
                        if(i1 != 0){ 
                        inimigo.remove();
                        x=inimigo.size1();
                        i1=inimigo.size1();
                        }
                        break;
                    case 2:
                        if(i2 != 0){ 
                        inimigo.remove2();
                        x=inimigo.size2();
                        i2=inimigo.size2();
                        }
                        break;
                    case 3:
                        if(i3 != 0){ 
                        inimigo.remove3();
                        x=inimigo.size3();
                        i3=inimigo.size3();}
                        break;
                    case 4:
                        if(i4 != 0){ 
                        inimigo.remove4();
                        x=inimigo.size4();
                        i4=inimigo.size4();}
                        break;
                }
        } 
        public void removerA(){
                switch(valorA)
                {
                    case 1:
                        if(q1 != 0){aliado1.remove(aliado1.size()-1);
                        personagem1.Diminuir();
                        q1= aliado1.size();
                        x=aliado1.size(); }
                        
                        break;
                    case 2:
                        if(q2 != 0){aliado2.remove(aliado2.size()-1);
                        personagem2.Diminuir();
                        q2= aliado2.size();
                        x=aliado2.size(); }
                        
                        break;
                    case 3:
                        if(q3 != 0){aliado3.remove(aliado3.size()-1);
                        personagem3.Diminuir();
                        q3= aliado3.size();
                        x=aliado3.size(); }
                        
                        break;
                    case 4:
                        if(q4 != 0){aliado4.remove(aliado4.size()-1);
                        personagem4.Diminuir();
                        q4= aliado4.size();
                        x=aliado4.size(); }
                        
                        break;
                }         
        }  
        void vivo(){
        if(q1>=1){a=0;}
        else if(q2>=1){a=1;}
        else if(q3>=1){a=2;}
        else if(q4>=1){a=3;}
        }
        /**
     * Método que junta todos as funções para execução da batalha.
     * @param event. 
     */
        @FXML public void Começar(ActionEvent event) {
        while(q1 > 0 || q2> 0 || q3 > 0 || q4 > 0){
        this.vivo();
        verificador=3;
        switch(a)
        {
            case 0:
                    System.out.println("Guerreiros");
                    do{
                        this.inicializar11();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    if(q2>=1){a=1; break;}
                    else if(q3>=1){a=2;break;}
                    else if(q4>=1){a=3;break;}
                    System.out.println("Arqueiras");
                    do{
                        this.inicializar12();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    if(q2>=1){a=1; break;}
                    else if(q3>=1){a=2;break;}
                    else if(q4>=1){a=3;break;}
                    System.out.println("Medicos");
                    do{
                        this.inicializar13();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    if(q2>=1){a=1; break;}
                    else if(q3>=1){a=2;break;}
                    else if(q4>=1){a=3;break;}
                    System.out.println("Infiltradoras");
                    do{
                        this.inicializar14();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    if(q2>=1){a=1; break;}
                    else if(q3>=1){a=2;break;}
                    else if(q4>=1){a=3;break;}
                    break;
                
            case 1:
                    System.out.println("Guerreiros");
                    do{
                        this.inicializar21();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    if(q3>=1){a=2;break;}
                    else if(q4>=1){a=3;break;}
                    System.out.println("Arqueiras");
                    do{
                        this.inicializar22();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    if(q3>=1){a=2;break;}
                    else if(q4>=1){a=3;break;}
                    System.out.println("Medicos");
                    do{
                        this.inicializar23();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    if(q3>=1){a=2;break;}
                    else if(q4>=1){a=3;break;}
                    System.out.println("Infiltradoras");
                    do{
                        this.inicializar24();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    if(q2>=1){a=1; break;}
                    else if(q3>=1){a=2;break;}
                    else if(q4>=1){a=3;break;}
                break;
                case 2:
                    System.out.println("Guerreiros");
                    do{
                        this.inicializar31();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    if(q4>=1){a=3;break;}
                    System.out.println("Arqueiras");
                    do{
                        this.inicializar32();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    if(q4>=1){a=3;break;}
                    System.out.println("Medicos");
                    do{
                        this.inicializar33();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    if(q4>=1){a=3;break;}
                    System.out.println("Infiltradoras");
                    do{
                        this.inicializar34();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    if(q4>=1){a=3;break;}
                break;  
            case 3:
                    System.out.println("Guerreiros");
                    do{
                        this.inicializar41();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    System.out.println("Arqueiras");
                    do{
                        this.inicializar42();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    System.out.println("Medicos");
                    do{
                        this.inicializar43();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                    System.out.println("Infiltradoras");
                    do{
                        this.inicializar44();
                        do{
                            this.batalha();
                        }while(verificador == 3);                    
                        this.resultado();
                    }while(x>0);
                    vivo();
                break;
        }
        }
        
        this.resultado();
        
    }
}
