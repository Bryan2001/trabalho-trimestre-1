package TrabalhoTrimestre1;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

public class Personagem1Controller implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
     /**
     * Método que chama a tela de modificação de ´personagem.
     */
    public void Modificar_personagem1() throws IOException{
        Play.trocaTela("Personagem1_Modificar.fxml");
    }
     /**
     * Método de volta de tela.
     */
    public void Voltar() throws IOException{
        Play.trocaTela("TelaInicial.fxml");
    }
}
